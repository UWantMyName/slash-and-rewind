﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
	public GameObject MainPanel;
	public GameObject AboutPanel;

    public void StartButton_Click()
	{
		SceneManager.LoadScene(1);
	}

	public void AboutButton_Click()
	{
		MainPanel.SetActive(false);
		AboutPanel.SetActive(true);
	}

	public void BackButton_Click()
	{
		MainPanel.SetActive(true);
		AboutPanel.SetActive(false);
	}
}
