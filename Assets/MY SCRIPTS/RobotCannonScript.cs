﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotCannonScript : MonoBehaviour
{
	public float fireCooldown = 2f;
	public GameObject Projectile;

	public void StartShooting()
	{
		StartCoroutine(Shoot());
	}

	public void StopShooting()
	{
		StopCoroutine(Shoot());
		StopAllCoroutines();
	}

    private IEnumerator Shoot()
	{
		yield return new WaitForSeconds(fireCooldown);
		GameObject go = Instantiate(Projectile, transform.position, Quaternion.identity);
		go.GetComponent<RobotProjectileScript>().TransferDirection(transform.forward);
		StartCoroutine(Shoot());
	}
}
