﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting.APIUpdating;

public class RobotProjectileScript : MonoBehaviour
{
    public float speed = 5f;
	public float rotSpeed = 3f;
	public Vector3 direction;
	public GameObject DeathEffect;
	private bool dead = false;

	public void TransferDirection(Vector3 dir)
	{
		direction = dir;
	}

	private void Move()
	{
		transform.position += direction * speed * Time.deltaTime * Time.timeScale;
	}

	private void OnColisionEnter(Collision col)
	{
		Debug.Log(col);
	}

	private void Rotate()
	{
		transform.Rotate(new Vector3(rotSpeed / 2 * Time.deltaTime, rotSpeed * Time.deltaTime, rotSpeed * Time.deltaTime));
	}

	public void Die()
	{
		dead = true;
		GetComponent<MeshRenderer>().enabled = false;
		GetComponent<BoxCollider>().enabled = false;
		StartCoroutine(DieAfter());
	}

	private IEnumerator DieAfter()
	{
		GameObject go = Instantiate(DeathEffect, transform.position, Quaternion.identity);
		yield return new WaitForSeconds(DeathEffect.GetComponent<ParticleSystem>().main.duration);
		Destroy(go);
		Destroy(gameObject);
	}

    private void Update()
	{
		if (!dead)
		{
			Move();
			Rotate();
		}
	}
}
