﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSc : MonoBehaviour
{
    private FirstPersonAIO FP;
	public ParticleSystem SpeedPS;
	public GameObject DeathPanel;
	public SwordScript sword;
	public SlowMotionControllerScript slowmotion;
	public RewindControllerScript rewind;
	public DoorScript[] doors;
	private GameObject SliceText;
	[SerializeField] private Text HPtext;
	public GameObject[] robots;
	public bool killedAll = false;
	public bool dead = false;
	private Vector3 oldPosition;
	private float speed = 0f;
	public AudioClip DeathSound;
	private int HealthPoints = 100;

    private void Start()
	{
		oldPosition = transform.position;
		Time.timeScale = 1f; // to prevent from slowing down when reloading the level
		SliceText = GameObject.Find("MAIN CANVAS/SLICE PANEL/SliceText");
		FP = GetComponent<FirstPersonAIO>();
	}

    private void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "RobotProjectile" && !rewind.isReversing)
		{
			other.gameObject.GetComponent<RobotProjectileScript>().Die();
			HealthPoints -= 5;
			if (HealthPoints <= 0 && !dead)
			{
				HealthPoints = 0;
				GetComponent<AudioSource>().clip = DeathSound;
				GetComponent<AudioSource>().Play();
				dead = true;
				FP.enabled = false;
				sword.enabled = false;
				slowmotion.deadPlayer = true;
				Time.timeScale = 0f;
				DeathPanel.SetActive(true);
			}
		}
	}

	private void UpdateSliceText()
	{
		SliceText.SetActive(false);
		killedAll = true;
		foreach (GameObject go in robots)
			if (go != null)
			{
				float dist = Vector3.Distance(go.transform.position, transform.position);
				if (go.GetComponent<RobotScript>().IsVisible() && (1.4f <= dist && dist <= 3.73f))
				{
					SliceText.SetActive(true);
					killedAll = false;
				}
			}
	}

	private void UpdateSpeedAndEffect()
	{
		speed = Vector3.Distance(transform.position, oldPosition) / Time.deltaTime;

		if (speed > 2500f) SpeedPS.Play();
		else SpeedPS.Stop();
	}

	private void Update()
	{
		UpdateSliceText();
		HPtext.text = HealthPoints.ToString();
		UpdateSpeedAndEffect();
	}
}
