﻿using Microsoft.Win32.SafeHandles;
using UnityEngine;

public class PointInTime
{
    public Vector3 position;
    public Vector3 rotation;

    public PointInTime(Vector3 pos, Vector3 rot)
	{
        this.position = pos;
        this.rotation = rot;
	}
}
