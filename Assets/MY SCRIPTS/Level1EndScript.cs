﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level1EndScript : MonoBehaviour
{
	[SerializeField] private GameObject SpeedPS;
	[SerializeField] private GameObject RewindPS;
	[SerializeField] private GameObject EndPanel;
	[SerializeField] private FirstPersonAIO Player;
	[SerializeField] private SwordScript sword;

	[SerializeField] private AnimationClip EndPanelAnimation;
	private bool canDetect = false;

    private void OnCollisionEnter(Collision col)
	{
		if (col.gameObject.tag == "Player" && col.gameObject.GetComponent<PlayerSc>().killedAll)
		{
			Player.enabled = false;
			sword.enabled = false;
			SpeedPS.SetActive(false);
			RewindPS.SetActive(false);
			EndPanel.SetActive(true);
			StartCoroutine(Fade());
		}
	}

	private IEnumerator Fade()
	{
		yield return new WaitForSeconds(EndPanelAnimation.length);
		canDetect = true;
	}

	private void FixedUpdate()
	{
		if (canDetect)
		{
			if (SceneManager.GetActiveScene().name != "Level 4")
			{
				if (Input.GetMouseButtonDown(0))
				{
					if (SceneManager.GetActiveScene().name == "Level 1")
						SceneManager.LoadScene("Level 2");
					if (SceneManager.GetActiveScene().name == "Level 2")
						SceneManager.LoadScene("Level 3");
					if (SceneManager.GetActiveScene().name == "Level 3")
						SceneManager.LoadScene("Level 4");
				}
				else if (Input.GetKeyDown(KeyCode.Escape)) Debug.Log("Main Menu");
			}
			else
			{
				if (Input.anyKeyDown)
					SceneManager.LoadScene("Main Menu");
			}
		}
	}
}
