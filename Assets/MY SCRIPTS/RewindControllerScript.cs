﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewindControllerScript : MonoBehaviour
{
    private GameObject player;
    private List<PointInTime> pointsInTime;
    public bool isReversing = false;
    public float recordTime = 5f;

    public GameObject SpeedPS;
    public GameObject RewindPS;

    void Start()
    {
        player = GameObject.Find("PLAYER");
        pointsInTime = new List<PointInTime>();
    }

    private void Record()
	{
        RewindPS.SetActive(false);
        SpeedPS.SetActive(true);
        if (pointsInTime.Count > Mathf.Round(recordTime / Time.fixedDeltaTime))
            pointsInTime.RemoveAt(pointsInTime.Count - 1);

        pointsInTime.Insert(0, new PointInTime(player.transform.position, player.transform.eulerAngles));
	}

    private void Rewind()
	{
        if (pointsInTime.Count > 1)
        {
            RewindPS.SetActive(true);
            SpeedPS.SetActive(false);
            PointInTime pointsintime = pointsInTime[0];
            player.transform.position = pointsintime.position;
            player.transform.rotation = new Quaternion(pointsintime.rotation.x, pointsintime.rotation.y, pointsintime.rotation.z, 1.0f);
            pointsInTime.RemoveAt(0);
        }
        else isReversing = false;
	}

    private void FixedUpdate()
    {
        if (!isReversing) Record();
        else Rewind();
    }
}
