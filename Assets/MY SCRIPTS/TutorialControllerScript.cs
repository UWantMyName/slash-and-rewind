﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class TutorialControllerScript : MonoBehaviour
{
    [SerializeField] private AnimationClip FadeIn;
    [SerializeField] private FirstPersonAIO Player;
    [SerializeField] private Animator TutorialPanel;
    [SerializeField] private SwordScript sword;
    [SerializeField] private GameObject SpeedPS;

    private bool startedGame = false;
    private bool canDetect = false;

    private void Start()
	{
        Player.enabled = false;
        sword.enabled = false;
        StartCoroutine(WaitTillCanDetect());
	}

    private IEnumerator WaitTillCanDetect()
	{
        yield return new WaitForSeconds(FadeIn.length);
        canDetect = true;
	}

    private void EnableGame()
	{
        TutorialPanel.SetBool("Ok", true);
        Player.enabled = true;
        sword.enabled = true;
        SpeedPS.SetActive(true);
    }

    private void FixedUpdate()
	{
        if (canDetect)
		{
            if (Input.anyKeyDown && !startedGame)
            {
                startedGame = true;
                EnableGame();
            }
        }
	}
}
