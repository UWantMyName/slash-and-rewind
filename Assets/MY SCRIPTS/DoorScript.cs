﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class DoorScript : MonoBehaviour
{
	public int RobotsLeft;
	private bool destroyedAll = false;
	private AudioSource AS;

	private void Start()
	{
		AS = GetComponent<AudioSource>();
	}

	public void RobotKilled()
	{
		RobotsLeft--;
	}

	private void Update()
	{
		if (!destroyedAll && RobotsLeft == 0)
		{
			destroyedAll = true;
			AS.Play();
			GetComponent<Animator>().enabled = true;
		}
	}
}
