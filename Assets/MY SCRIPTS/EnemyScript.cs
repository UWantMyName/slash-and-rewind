﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
	public float force = 50f;

	public void Hit(Vector3 direction)
	{
		GetComponent<Rigidbody>().AddForce(direction * force, ForceMode.Impulse);
	}
}
