﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordScript : MonoBehaviour
{
    private Animator anim;
    private AudioSource AS;
  
    public GameObject Trail;
    public AudioClip Slice;
    public AudioClip SliceAndHit;
    public GameObject sword;

    public float force = 10f;
    public bool attacking = false;

    private void Start()
	{
        anim = GetComponent<Animator>();
        AS = GetComponent<AudioSource>();
	}

    public void DisableAttackAnimation()
    {
        attacking = false;
        anim.SetBool("Attack", false);
    }

    public void DisableTrails()
	{
        Trail.SetActive(false);
	}

    public void OnCollisionEnter(Collision collider)
	{
        if (collider.gameObject.tag == "RobotPart")
            if (attacking)
                collider.gameObject.GetComponent<RobotPartScript>().Hit(sword.transform.forward, force);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !attacking)
		{
            AS.clip = SliceAndHit;
            AS.Play();
            attacking = true;
            anim.SetBool("Attack", true);
            Trail.SetActive(true);
        }
    }
}
