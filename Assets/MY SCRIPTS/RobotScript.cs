﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotScript : MonoBehaviour
{
	public DoorScript door;

	[SerializeField] private GameObject[] LA;
	[SerializeField] private GameObject[] RA;
	[SerializeField] private GameObject[] HEAD;
	[SerializeField] private GameObject[] LL;
	[SerializeField] private GameObject[] RL;
	[SerializeField] private GameObject[] BODY;

	public float rotSpeed = 10f;
	public float distanceToDetectFrom = 10f;

	public RobotCannonScript Left;
	public RobotCannonScript Right;
	private GameObject Player;
	private SlowMotionControllerScript slowmotionController;
	private RewindControllerScript rewindController;

	public float deathTime = 3f;
	public bool dead = false;
	private bool played = false;
	public bool startedShooting = false;

	private void Start()
	{
		startedShooting = false;
		rewindController = GameObject.Find("REWIND CONTROLLER").GetComponent<RewindControllerScript>();
		Player = GameObject.Find("PLAYER");
		slowmotionController = GameObject.Find("SLOWMOTION CONTROLLER").GetComponent<SlowMotionControllerScript>();
	}

	private void ChangeRigidody(GameObject go, float force, Vector3 dir)
	{
		Rigidbody rb = go.GetComponent<Rigidbody>();
		rb.useGravity = true;
		rb.isKinematic = false;
		rb.constraints = RigidbodyConstraints.None;
		rb.AddForce(dir * (force / 3), ForceMode.Impulse);
	}

	public void DetachAll(float force, Vector3 dir)
	{
		if (!dead)
		{
			dead = true;
			if (!gameObject.tag.Contains("Unrewind")) rewindController.isReversing = true;
			door.RobotKilled();
			Left.StopShooting();
			Right.StopShooting();
			slowmotionController.StartSlowMotion();
		}

		foreach (GameObject go in LA)
		{
			ChangeRigidody(go, force, dir);
			Physics.IgnoreCollision(go.GetComponent<Collider>(), Player.GetComponent<Collider>());
			//go.GetComponent<RobotPartScript>().StartFadind();
		}

		foreach (GameObject go in RA)
		{
			ChangeRigidody(go, force, dir);
			Physics.IgnoreCollision(go.GetComponent<Collider>(), Player.GetComponent<Collider>());
			//go.GetComponent<RobotPartScript>().StartFadind();
		}

		foreach (GameObject go in HEAD)
		{
			ChangeRigidody(go, force, dir);
			Physics.IgnoreCollision(go.GetComponent<Collider>(), Player.GetComponent<Collider>());
			//go.GetComponent<RobotPartScript>().StartFadind();
		}

		foreach (GameObject go in LL)
		{
			ChangeRigidody(go, force, dir);
			Physics.IgnoreCollision(go.GetComponent<Collider>(), Player.GetComponent<Collider>());
			//go.GetComponent<RobotPartScript>().StartFadind();
		}

		foreach (GameObject go in RL)
		{
			ChangeRigidody(go, force, dir);
			Physics.IgnoreCollision(go.GetComponent<Collider>(), Player.GetComponent<Collider>());
			//go.GetComponent<RobotPartScript>().StartFadind();
		}

		foreach (GameObject go in BODY)
		{
			ChangeRigidody(go, force, dir);
			Physics.IgnoreCollision(go.GetComponent<Collider>(), Player.GetComponent<Collider>());
			//go.GetComponent<RobotPartScript>().StartFadind();
		}

		StartCoroutine(Die());
	}

	private IEnumerator Die()
	{
		if (!played)
		{
			GetComponent<AudioSource>().Play();
			played = true;
		}
		yield return new WaitForSeconds(deathTime);
		Destroy(gameObject);
	}

	private void LookAtPlayer()
	{
		Vector3 targetDirection = transform.position - Player.transform.position;
		float singlestep = rotSpeed * Time.deltaTime;
		Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, singlestep, 0.0f);
		transform.rotation = Quaternion.LookRotation(newDirection);
	}

	public bool IsVisible()
	{
		if (dead) return false;
		foreach (GameObject go in LA)
			if (go.GetComponent<Renderer>().isVisible) return true;

		foreach (GameObject go in RA)
			if (go.GetComponent<Renderer>().isVisible) return true;

		foreach (GameObject go in HEAD)
			if (go.GetComponent<Renderer>().isVisible) return true;

		foreach (GameObject go in LL)
			if (go.GetComponent<Renderer>().isVisible) return true;

		foreach (GameObject go in RL)
			if (go.GetComponent<Renderer>().isVisible) return true;

		foreach (GameObject go in BODY)
			if (go.GetComponent<Renderer>().isVisible) return true;
		return false;
	}

	private bool VisibleonCamera()
	{
		RaycastHit hit = new RaycastHit();
		Vector3 dir = Player.transform.position - transform.position;
		if (Physics.Raycast(transform.position, dir, out hit))
			if (hit.transform.name == "PLAYER") return true;
		return false;
	}

	private void Update()
	{
		if (!dead)
		{
			if (Vector3.Distance(transform.position, Player.transform.position) <= distanceToDetectFrom)
			{
				LookAtPlayer();
				if (Player.GetComponent<PlayerSc>().dead)
				{
					startedShooting = false;
					Left.StopShooting();
					Right.StopShooting();
				}
				else
				{
					if (gameObject.tag.Contains("Shoot") && !startedShooting && VisibleonCamera())
					{
						Debug.Log(".");
						startedShooting = true;
						Left.StartShooting();
						Right.StartShooting();
					}
				}
			}
			else
			{
				startedShooting = false;
				Left.StopShooting();
				Right.StopShooting();
			}
		}
		
	}
}
