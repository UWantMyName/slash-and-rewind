﻿using UnityEngine;

public class SlowMotionControllerScript : MonoBehaviour
{
    public float slowDownFactor = 0.05f;
    public float slowDownLength = 2f;
	public AudioSource AS1;
	public bool deadPlayer = false;

    public void StartSlowMotion()
	{
        Time.timeScale = slowDownFactor;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
	}

	private void Update()
	{
		if (!deadPlayer)
		{
			Time.timeScale += (1f / slowDownLength) * Time.unscaledDeltaTime;
			AS1.pitch = Time.timeScale;
			Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
			AS1.pitch = Mathf.Clamp(AS1.pitch, 0.75f, 1f);
		}
	}
}
