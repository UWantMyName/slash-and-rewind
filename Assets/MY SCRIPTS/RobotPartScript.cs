﻿using System.Collections;
using UnityEngine;

public class RobotPartScript : MonoBehaviour
{
	public RobotScript robot;
	private Rigidbody rb;
	private bool hit = false;
	public Shader shader;
	private float dissolveAmount = 0f;
	private bool startedFading = false;

	private void OnCollisionEnter(Collision collision)
	{
		if ((collision.gameObject.tag == "PlayerBlade" || collision.gameObject.tag == "BladePE") && !hit && collision.gameObject.GetComponent<SwordScript>().attacking)
			hit = true;
	}

	public void Hit(Vector3 dir, float force)
	{
		if (!hit)
		{
			hit = true;
			rb = GetComponent<Rigidbody>();
			rb.useGravity = true;
			rb.isKinematic = false;
			rb.AddForce(dir * force, ForceMode.Impulse);
			rb.constraints = RigidbodyConstraints.None;
			robot.DetachAll(force / 2, dir);
			StartFadind();
		}
	}

	public void StartFadind()
	{
		if (!startedFading)
		{
			startedFading = true;
			dissolveAmount = 0f;
			StartCoroutine(FadeOut());
		}
	}

	private IEnumerator FadeOut()
	{
		dissolveAmount += 2 * Time.deltaTime;
		GetComponent<Renderer>().sharedMaterial.SetFloat("DissolveAmount", dissolveAmount);
		yield return new WaitForSeconds(Time.deltaTime);
		if (dissolveAmount < 1f) StartCoroutine(FadeOut());
		else dissolveAmount = 1f;
	}
}
