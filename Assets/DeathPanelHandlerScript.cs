﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathPanelHandlerScript : MonoBehaviour
{
	private bool detect = false;

	private void Start() { StartCoroutine(Wait()); }

	private IEnumerator Wait()
	{
		yield return new WaitForSecondsRealtime(1f);
		detect = true;
	}

    private void Update()
	{
		if (detect && Input.GetMouseButtonDown(0))
		{
			int scene = SceneManager.GetActiveScene().buildIndex;
			SceneManager.LoadScene(scene, LoadSceneMode.Single);
		}
	}
}
