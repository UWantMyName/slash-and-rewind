﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StoryHandlerScript : MonoBehaviour
{
    private string part1 = "In a laboratory deep under an endless dessert, lives a lunatic that works on a time-rewinding machine.";
    private string part2 = "His creation was close to coming to life. But no idea did he have when his fortress was visited by unwanted guests.";
    private string part3 = "An army of robots coming from an unknown dimension, to destroy what the scientist has created. To deny him putting his hands on technology that would change the course of Earth.";
    private string part4 = "Alarms at high volume, but the mad scientist would not give up. But he knew why. His work could not cease. He would not let all his work come to a cruel end.";
    private string part5 = "He has faith. In his childhood creation: Etunim. A fearless ninja robot built for the protection of its master.";
    private string part6 = "You are under his control. You are now Etunim. Protect your creator and his work from evil hands!";

    private string current = " ";
    private int index = 0;
    private int storyIndex = 0;
    private bool animating = false;

    [SerializeField] private float letterCooldown = 0.05f;
    [SerializeField] private Text storyline;
    [SerializeField] private GameObject PlayButton;
    [SerializeField] private GameObject NextButton;

    private void Start()
	{
        current = part1;
        index = 1;
        animating = true;
        storyline.text = "";
        StartCoroutine(AnimateText());
	}

    private IEnumerator AnimateText()
	{
        storyline.text += current[storyIndex];
        yield return new WaitForSeconds(letterCooldown);
        if (storyIndex < current.Length - 1)
        {
            storyIndex++;
            StartCoroutine(AnimateText());
        }
        else
		{
            if (index == 6)
			{
                NextButton.SetActive(false);
                PlayButton.SetActive(true);
            }                
            animating = false;
        }
	}

    private void UpdateText()
	{
        NextButton.SetActive(true);
        PlayButton.SetActive(false);

        if (index == 1) current = part1;
        else if (index == 2) current = part2;
        else if (index == 3) current = part3;
        else if (index == 4) current = part4;
        else if (index == 5) current = part5;
        else if (index == 6) current = part6;
        else if (index > 6)
		{
            NextButton.SetActive(false);
            PlayButton.SetActive(true);
		}

        if (1 <= index && index <= 6)
		{
            storyline.text = "";
            storyIndex = 0;
            animating = true;
            StartCoroutine(AnimateText());
        }
	}

    public void NextButton_Click()
	{
        if (animating)
        {
            animating = false;
            storyline.text = current;
            StopAllCoroutines();
            if (index == 6)
			{
                NextButton.SetActive(false);
                PlayButton.SetActive(true);
            }
        }
        else
		{
            index++;
            if (index >= 6) index = 6;
            UpdateText();
        }
	}

    public void BackButton_Click()
	{
        if (animating)
        {
            animating = false;
            storyline.text = current;
            StopAllCoroutines();
        }
        else
        {
            index--;
            if (index < 1) index = 1;
            UpdateText();
        }
    }

    public void PlayButton_Click()
	{
        SceneManager.LoadScene("Level 1");
	}
}
